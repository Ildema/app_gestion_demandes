const prompt = require("prompt-sync")();

class Produit {
    constructor(libelle, prix, quantite, codeTVA) {
        this.libelle = libelle;
        this.prix = prix;
        this.quantite = quantite;
        this.codeTVA = codeTVA;
    }
    prixTotal() {
        return this.prix * this.quantite
    }
}

let listeProduits = [];
const reductions = new Map();
reductions.set(1000, 0.03)
reductions.set(5000, 0.05)
reductions.set(7000, 0.07)
reductions.set(10000, 0.1)
reductions.set(50000, 0.15)
console.log(reductions);

const txTVA = new Map();
txTVA.set('FR', 0.2)
txTVA.set('DE', 0.19)
txTVA.set('BE', 0.21)
txTVA.set('ES', 0.25)
txTVA.set('GB', 0.2)
txTVA.set('IT', 0.22)
console.log(txTVA);

var montant = 0;
var stop = 1;


var test = new Produit();

while(stop != 2){
    console.log("Voulez vous ajouter un article ?\n1 - oui\n2 - non")
    stop = prompt();
    if(stop == 1){
        addNewProduit();
    } else if(stop == 2){
        if (listeProduits.length != 0){
            console.log("Quel est le montant hors-taxe ?");
            var montantHT = prompt()
            
            if (montantHT >= 50000){
                console.log(`Souhaitez-vous une réduction de ${reductions.get(50000)*100}% ?\n1 - oui\n2 - non`);
                var resp = prompt()
                if (resp = 1 ) {
                    montantHT = montantHT - montantHT*(reductions.get(50000))
                }
            } else if (montantHT >= 10000){
                console.log(`Souhaitez-vous une réduction de ${reductions.get(10000)*100}% ?\n1 - oui\n2 - non`);
                var resp = prompt()
                if (resp = 1 ) {
                    montantHT = montantHT - montantHT*(reductions.get(10000))
                }
            } else if(montantHT >= 7000){
                console.log(`Souhaitez-vous une réduction de ${reductions.get(7000)*100}% ?\n1 - oui\n2 - non`);
                var resp = prompt()
                if (resp = 1 ) {
                    montantHT = montantHT - montantHT*(reductions.get(7000))
                }
            } else if (montantHT >= 5000){
                console.log(`Souhaitez-vous une réduction de ${reductions.get(5000)*100}% ?\n1 - oui\n2 - non`);
                var resp = prompt()
                if (resp = 1 ) {
                    montantHT = montantHT - montantHT*(reductions.get(5000))
                }
            } else if (montantHT >= 1000) {
                console.log(`Souhaitez-vous une réduction de ${reductions.get(1000)*100}% ?\n1 - oui\n2 - non`);
                var resp = prompt()
                if (resp = 1 ) {
                    montantHT = montantHT - montantHT*(reductions.get(1000))
                }
            }

            console.log(montantHT)

            console.log("Quel est le montant de la TVA ?");
            var laTVA = prompt()

            console.table(listeProduits)
            var table = [["Montant HT", montantHT],
                           ["TVA", laTVA],
                           ["Montant TTC", calculTTC(parseInt(montantHT), parseInt(laTVA))]];
            console.table(table);
        }
        console.log("fin du programme");
    } else {
        console.log("Choix invalide");
    }
}


function addNewProduit(){
    let libelle;
    let prix;
    let quantite;
    let codeTVA;
    console.log("Libelle du produit ?");
    libelle = prompt();
    console.log("Prix du produit ?");
    prix = prompt();
    console.log("Quantite du produit ?");
    quantite = prompt();
    console.log("CodeTVA du produit ?");
    codeTVA = prompt();
    var produit = new Produit(libelle, prix, quantite, codeTVA);
    listeProduits.push(produit);
}

// function calculHT(){
//     return listeProduits.reduce((acc, product) => acc + product.prixTotal(), 0)
// }

function calculTTC(totalHT, tva){
    return totalHT + tva;
}